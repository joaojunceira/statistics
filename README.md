# Statistics App

## Requirements
* JDK 11
* Scala 2.13
* Sbt 1.4

## How to Compile and run tests

`sbt clean compile test`

## How to Run

After compile the app execute:
1. `sbt run`
2. Insert the commands
Eg. `ADD http://www.rte.ie/news/politics/2018/1004/1001034-cso/ 20`

## Design Decisions

Some considerations around Design decisions.
* This is a Command Line App so data is being stored in memory and it's assuming there will be no concurrent requests
* The Data Model created to store the data is a Key Value where Domain is the key. The reason for this is to optimize the speed of aggregation process

Possible improvements:
* `CommandLineInterpreter` contains the mapping between commands keyword and a specific function that will be executed. If the number of commands increase then might be good idea to extract this functions into separated classes and apply Strategy Pattern
* Messages could be extracted to a configuration file
