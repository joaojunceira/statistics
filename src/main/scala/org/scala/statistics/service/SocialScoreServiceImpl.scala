package org.scala.statistics.service

import org.scala.statistics.helper.Helper.UrlToDomain
import org.scala.statistics.model.{Report, UrlScore}
import org.scala.statistics.repository.SocialScoreRepository

import java.net.URL

class SocialScoreServiceImpl(val socialScoreRepository: SocialScoreRepository) extends SocialScoreService {

  override def addUrl(url: URL, socialScore: Int): Unit = {
    val domain = url.toDomain.getOrElse("")
    socialScoreRepository.save(domain, UrlScore(url, socialScore))
  }

  override def removeUrl(url: URL): Unit = {
    val domain = url.toDomain.getOrElse("")
    socialScoreRepository.deleteByDomainAndUrl(domain, url)
  }

  override def exportReport(): Report = {
    Report(socialScoreRepository.aggregationOfSocialScoreByDomain())
  }
}
