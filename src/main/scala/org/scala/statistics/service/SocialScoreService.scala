package org.scala.statistics.service

import org.scala.statistics.model.Report

import java.net.URL

trait SocialScoreService {
  def addUrl(url: URL, socialScore: Int)

  def exportReport(): Report

  def removeUrl(url: URL): Unit
}
