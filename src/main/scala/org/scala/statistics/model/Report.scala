package org.scala.statistics.model

import com.fasterxml.jackson.annotation.{JsonProperty, JsonPropertyOrder}

case class Report(domainScores: List[DomainSocialScore])

@JsonPropertyOrder(Array(
  "domain",
  "urls",
  "social_score"
))
case class DomainSocialScore(@JsonProperty("domain") domain: String,
                             @JsonProperty("urls") urls: Int,
                             @JsonProperty("social_score") socialScore: Int)
