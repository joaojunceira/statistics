package org.scala.statistics.model

import java.net.URL

case class UrlScore(url: URL, score: Int)
