package org.scala.statistics.interpreter

import org.scala.statistics.exception.NotFoundException
import org.scala.statistics.helper.Helper.{ConvertToCsv, ValidateString}
import org.scala.statistics.helper.Messages._
import org.scala.statistics.repository.SocialScoreRepositoryInMemory
import org.scala.statistics.service.{SocialScoreService, SocialScoreServiceImpl}

import java.net.URL

class CommandLineInterpreter(val socialScoreService: SocialScoreService =
                             new SocialScoreServiceImpl(new SocialScoreRepositoryInMemory())) extends Interpreter {

  override def execute(args: Array[String]): String = {
    if (args.length < 1) {
      return invalidInputMessage
    }
    val command = args(0).toLowerCase
    try {
      command match {
        case "add" => add(args)
        case "export" => export()
        case "remove" => remove(args)
        case _ => wrongCommandMessage
      }
    } catch {
      case e: NotFoundException => e.getMessage
      case e: Throwable => e.getMessage
    }
  }

  private def add(args: Array[String]): String = {
    if (args.length < 3) {
      return wrongInputAddMessage
    }
    if (args(1).isInvalidUrl) {
      return wrongInputUrlMessage
    }
    if (args(2).isNotOnlyDigits) {
      return wrongInputScoreMessage
    }
    socialScoreService.addUrl(url = new URL(args(1)), socialScore = args(2).toInt)
    successAddMessage
  }

  private def remove(args: Array[String]): String = {
    if (args.length < 2) {
      return wrongInputRemoveMessage
    }
    if (args(1).isInvalidUrl) {
      return wrongInputUrlMessage
    }
    socialScoreService.removeUrl(new URL(args(1)))
    successDeletedMessage
  }

  private def export(): String = {
    socialScoreService.exportReport().toCsv()
  }
}
