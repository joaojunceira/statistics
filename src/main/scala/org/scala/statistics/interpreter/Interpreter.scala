package org.scala.statistics.interpreter

trait Interpreter {
  def execute(args: Array[String]): String
}
