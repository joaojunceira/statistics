package org.scala.statistics

import org.scala.statistics.interpreter.{CommandLineInterpreter, Interpreter}

import scala.io.StdIn.readLine
import scala.util.control.Breaks.break

object Main extends App {
  val interpreter: Interpreter = new CommandLineInterpreter()
  while (true) {
    print(">")
    val input = readLine().split(" ").map(a => a.trim)
    if (input.nonEmpty && input(0).equalsIgnoreCase("q")) {
      break
    }
    println(interpreter.execute(input))
  }
}
