package org.scala.statistics.repository

import org.scala.statistics.exception.NotFoundException
import org.scala.statistics.model.{DomainSocialScore, UrlScore}

import java.net.URL
import scala.collection.mutable
import scala.collection.mutable.ListBuffer

class SocialScoreRepositoryInMemory extends SocialScoreRepository {
  private val hashMap = mutable.HashMap[String, ListBuffer[UrlScore]]()

  override def get(domain: String): List[UrlScore] = {
    hashMap.getOrElse(domain, ListBuffer.empty[UrlScore]).toList
  }

  override def save(domain: String, urlScore: UrlScore): Unit = {
    hashMap.updateWith(domain) {
      case Some(value: ListBuffer[UrlScore]) => Option(value.addOne(urlScore))
      case None => Option(ListBuffer(urlScore))
    }
  }

  override def deleteByDomainAndUrl(domain: String, url: URL): Unit = {
    val urls: ListBuffer[UrlScore] = hashMap.getOrElse(domain, ListBuffer.empty[UrlScore])
    if (!urls.exists(a => a.url == url)) {
      throw new NotFoundException(s"Url ${url.toString} not found")
    }
    val urlRemoved = urls.filterNot(a => a.url == url)
    if (urlRemoved.isEmpty) {
      hashMap.remove(domain)
    } else {
      hashMap.put(domain, urlRemoved)
    }
  }

  override def aggregationOfSocialScoreByDomain(): List[DomainSocialScore] = {
    hashMap.view.map { case (a, b) => getDomainSocialScore(a, b) }.toList
  }

  private def getDomainSocialScore(domain: String, urls: ListBuffer[UrlScore]): DomainSocialScore = {
    DomainSocialScore(domain = domain, urls = urls.distinctBy(_.url).size, socialScore = urls.foldLeft(0)(_ + _.score))
  }
}
