package org.scala.statistics.repository

import org.scala.statistics.model.{DomainSocialScore, UrlScore}

import java.net.URL

trait SocialScoreRepository {
  def get(domain: String): List[UrlScore]

  def save(domain: String, urlScore: UrlScore): Unit

  def aggregationOfSocialScoreByDomain(): List[DomainSocialScore]

  def deleteByDomainAndUrl(domain: String, url: URL): Unit
}
