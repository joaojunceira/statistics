package org.scala.statistics.helper

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.csv.{CsvMapper, CsvSchema}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.scala.statistics.model.{DomainSocialScore, Report}

import java.net.URL

object Helper {
  private val mapper = CsvMapper.csvBuilder().addModule(DefaultScalaModule).build()

  private val domainRegex = """[^\.]+\.[^\.]+$""".r
  private val numericRegex = "^\\d+$".r
  private val urlRegex =
    ("((http|https)://)(www.)?[a-zA-Z0-9@:%._\\+~#?&//=]{2,256}" +
      "\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)").r

  implicit class UrlToDomain(url: URL) {
    def toDomain: Option[String] = {
      domainRegex.findFirstIn(url.getHost)
    }
  }

  implicit class ValidateString(string: String) {
    def isNotOnlyDigits: Boolean = {
      !numericRegex.matches(string)
    }

    def isInvalidUrl: Boolean = {
      !urlRegex.matches(string)
    }
  }

  implicit class ConvertToCsv(report: Report) {

    def toCsv(): String = mapper.writer(CsvSchema.builder().
      addColumn("domain").addColumn("urls").
      addColumn("social_score").setColumnSeparator(';').setQuoteChar('\u0000').setUseHeader(true).build()).
      writeValueAsString(report.domainScores)
  }

}
