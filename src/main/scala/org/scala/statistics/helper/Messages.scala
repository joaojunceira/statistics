package org.scala.statistics.helper

object Messages {
  val wrongCommandMessage = "Invalid command choose one of the available options ADD, REMOVE or EXPORT"

  val invalidInputMessage = "Invalid command"

  val wrongInputUrlMessage = "Wrong Input. URL is invalid"

  val wrongInputAddMessage = "Wrong Input. ADD command should be ADD <URL> <SCORE>"

  val wrongInputScoreMessage = "Wrong Input. Score should be numeric (Eg. 2)"

  val wrongInputRemoveMessage = "Wrong Input. REMOVE command should be REMOVE <URL>"

  val successAddMessage = "Saved!"

  val successDeletedMessage = "Deleted!"
}
