package org.scala.statistics.service

import org.scala.statistics.exception.NotFoundException
import org.scala.statistics.helper.Helper.UrlToDomain
import org.scala.statistics.model.UrlScore
import org.scala.statistics.repository.SocialScoreRepositoryInMemory
import org.scalatest.BeforeAndAfterEach
import org.scalatest.funsuite.AnyFunSuite

import java.net.URL

class SocialScoreServiceTest extends AnyFunSuite with BeforeAndAfterEach {
  private var socialScoreService: SocialScoreService = _

  override def beforeEach(): Unit = {
    socialScoreService = new SocialScoreServiceImpl(new SocialScoreRepositoryInMemory())
  }

  test("add should be successful") {
    val urlScore = UrlScore(new URL("https://news.ycombinator.com/item?id=26426816"), 20)

    socialScoreService.addUrl(urlScore.url, urlScore.score)

    val result = socialScoreService.exportReport()
    val expectedSize = 1
    assert(expectedSize == result.domainScores.size)
    result.domainScores.collectFirst(a => {
      assert(1 == a.urls)
      assert(urlScore.score == a.socialScore)
      assert(urlScore.url.toDomain.orNull == a.domain)
    })
  }

  test("remove should succeed for existing Url") {
    val url = new URL("https://news.ycombinator.com/item?id=26426816")
    val urlScore = UrlScore(url, 20)
    socialScoreService.addUrl(urlScore.url, urlScore.score)

    assert(socialScoreService.exportReport().domainScores.nonEmpty)
    socialScoreService.removeUrl(url)
    assert(socialScoreService.exportReport().domainScores.isEmpty)
  }

  test("remove should fail for non-existing Url") {
    assertThrows[NotFoundException] {
      socialScoreService.removeUrl(new URL("https://www.google.com/drive"))
    }
  }

  test("empty repository should generate empty report") {
    val result = socialScoreService.exportReport()

    assert(result.domainScores.isEmpty)
  }

  test("one element per domain should generate successfully") {
    val urlScore = UrlScore(new URL("https://news.ycombinator.com/item?id=26426816"), 20)
    val urlScore2 = UrlScore(new URL("https://www.example.com/item?id=8384384"), 180)
    socialScoreService.addUrl(urlScore.url, urlScore.score)
    socialScoreService.addUrl(urlScore2.url, urlScore2.score)

    val result = socialScoreService.exportReport()
    val expectedSize = 2
    assert(expectedSize == result.domainScores.size)
    result.domainScores.find(a => a.domain == urlScore.url.toDomain.orNull).collectFirst(a => {
      assert(1 == a.urls)
      assert(20 == a.socialScore)
      assert(urlScore.url.toDomain.orNull == a.domain)
    })
    result.domainScores.find(a => a.domain == urlScore2.url.toDomain.orNull).collectFirst(a => {
      assert(1 == a.urls)
      assert(180 == a.socialScore)
      assert(urlScore2.url.toDomain.orNull == a.domain)
    })
  }

  test("more than one element per domain should generate successfully") {
    val urlScore = UrlScore(new URL("https://news.ycombinator.com/item?id=26426816"), 20)
    val urlScore2 = UrlScore(new URL("https://news.ycombinator.com/item?id=8384384"), 20)
    socialScoreService.addUrl(urlScore.url, urlScore.score)
    socialScoreService.addUrl(urlScore2.url, urlScore2.score)

    val result = socialScoreService.exportReport()
    val expectedSize = 1
    assert(expectedSize == result.domainScores.size)
    result.domainScores.collectFirst(a => {
      assert(2 == a.urls)
      assert(40 == a.socialScore)
      assert(urlScore.url.toDomain.orNull == a.domain)
    })
  }
}
