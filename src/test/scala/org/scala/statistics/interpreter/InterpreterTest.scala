package org.scala.statistics.interpreter

import org.scala.statistics.helper.Messages
import org.scala.statistics.repository.SocialScoreRepositoryInMemory
import org.scala.statistics.service.SocialScoreServiceImpl
import org.scalatest.BeforeAndAfterEach
import org.scalatest.funsuite.AnyFunSuite

class InterpreterTest extends AnyFunSuite with BeforeAndAfterEach {
  private var interpreter: Interpreter = _

  override def beforeEach(): Unit = {
    interpreter = new CommandLineInterpreter(
      new SocialScoreServiceImpl(new SocialScoreRepositoryInMemory()))
  }

  test("add valid url should succeed") {
    val args = Array("ADD", "http://www.rte.ie/news/politics/2018/1004/1001034-cso/", "20")

    val result = interpreter.execute(args)

    assert(Messages.successAddMessage == result)
  }

  test("add invalid url should fail") {
    val args = Array("ADD", "www.rte.ie/news/politics/2018/1004/1001034-cso/", "20")

    val result = interpreter.execute(args)

    assert(Messages.wrongInputUrlMessage == result)
  }

  test("remove valid url should succeed") {
    interpreter.execute(
      Array("ADD", "http://www.rte.ie/news/politics/2018/1004/1001034-cso/", "20"))
    val args = Array("REMOVE", "http://www.rte.ie/news/politics/2018/1004/1001034-cso/", "20")

    val result = interpreter.execute(args)

    assert(Messages.successDeletedMessage == result)
  }

  test("remove non-existing url should fail") {
    val url = "http://www.rte.ie/news/politics/2018/1004/1001034-cso/"
    val args = Array("REMOVE", url, "20")

    val result = interpreter.execute(args)

    assert(s"Url $url not found" == result)
  }

  test("generate report with example should succeed") {
    val example1 = List(Array("ADD", "http://www.rte.ie/news/politics/2018/1004/1001034-cso/", "20"),
      Array("ADD", "https://www.rte.ie/news/ulster/2018/1004/1000952-moanghan-mine/", "30"),
      Array("ADD", "http://www.bbc.com/news/world-europe-45746837", "10"))
    example1.foreach(a => {
      interpreter.execute(a)
    })

    val result = interpreter.execute(Array("EXPORT"))

    assert(Seq("domain;urls;social_score", "bbc.com;1;10", "rte.ie;2;50") == stringToSeq(result))

    interpreter.execute(Array("REMOVE", "https://www.rte.ie/news/ulster/2018/1004/1000952-moanghan-mine/"))
    val result2 = interpreter.execute(Array("EXPORT"))

    assert(Seq("domain;urls;social_score", "bbc.com;1;10", "rte.ie;1;20") == stringToSeq(result2))
  }

  test("generate report should return empty") {
    val result = interpreter.execute(Array("EXPORT"))

    assert(Seq("domain;urls;social_score") == stringToSeq(result))
  }

  private def stringToSeq(result: String): Seq[String] = {
    result.split('\n').map(a => a.trim.replaceAll("[^A-Za-z0-9;._]", "")).toSeq
  }
}
