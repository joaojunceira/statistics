package org.scala.statistics.repository

import org.scala.statistics.exception.NotFoundException
import org.scala.statistics.helper.Helper.UrlToDomain
import org.scala.statistics.model.UrlScore
import org.scalatest.BeforeAndAfterEach
import org.scalatest.funsuite.AnyFunSuite

import java.net.URL

class SocialScoreRepositoryTest extends AnyFunSuite with BeforeAndAfterEach {

  private var socialScoreRepository: SocialScoreRepository = _

  override def beforeEach(): Unit = {
    socialScoreRepository = new SocialScoreRepositoryInMemory()
  }

  test("saving should occur successfully") {
    val urlScore = UrlScore(new URL("https://news.ycombinator.com/item?id=26426816"), 20)
    val domain = urlScore.url.toDomain.get

    socialScoreRepository.save(domain, urlScore)

    val result = socialScoreRepository.get(domain)
    val expectedSize = 1
    assert(expectedSize == result.size)
    result.collectFirst(a => {
      assert(urlScore.url == a.url)
      assert(urlScore.score == a.score)
    })
  }

  test("delete of existing element should succeed") {
    val urlScore = UrlScore(new URL("https://news.ycombinator.com/item?id=26426816"), 20)
    val domain = urlScore.url.toDomain.get
    socialScoreRepository.save(domain, urlScore)

    val result = socialScoreRepository.get(domain)
    assert(result.nonEmpty)
    socialScoreRepository.deleteByDomainAndUrl(domain, urlScore.url)
    assert(socialScoreRepository.get(domain).isEmpty)
  }

  test("delete of non-existing element should fail with NotFoundException") {
    val urlScore = UrlScore(new URL("https://news.ycombinator.com/item?id=26426816"), 20)
    val domain = urlScore.url.toDomain.get

    assertThrows[NotFoundException] {
      socialScoreRepository.deleteByDomainAndUrl(domain, urlScore.url)
    }
  }

  test("empty repository should generate empty report") {
    val result = socialScoreRepository.aggregationOfSocialScoreByDomain()

    assert(result.isEmpty)
  }

  test("one element per domain should generate successfully") {
    val urlScore = UrlScore(new URL("https://news.ycombinator.com/item?id=26426816"), 20)
    val urlScore2 = UrlScore(new URL("https://www.google.com/drive"), 20)
    val domain = urlScore.url.toDomain.get
    socialScoreRepository.save(domain, urlScore)
    val domain2 = urlScore2.url.toDomain.get
    socialScoreRepository.save(domain2, urlScore2)

    val result = socialScoreRepository.aggregationOfSocialScoreByDomain()

    val expectedSize = 2
    assert(expectedSize == result.size)
    assert(result.exists(a => a.domain == domain && a.socialScore == urlScore.score))
    assert(result.exists(a => a.domain == domain2 && a.socialScore == urlScore2.score))
  }

  test("more than one element per domain should generate successfully") {
    val urlScore1 = UrlScore(new URL("https://www.google.com/drive"), 20)
    val urlScore2 = UrlScore(new URL("https://www.google.com/drive"), 20)
    val domain = urlScore1.url.toDomain.get
    socialScoreRepository.save(domain, urlScore1)
    socialScoreRepository.save(domain, urlScore2)

    val result = socialScoreRepository.aggregationOfSocialScoreByDomain()

    val expectedSize = 1
    val expectedUrls = 1
    val expectedScore = 40
    assert(expectedSize == result.size)
    result.collectFirst(a => {
      assert(domain == a.domain)
      assert(expectedUrls == a.urls)
      assert(expectedScore == a.socialScore)
    })
  }
}
