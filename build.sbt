name := "statistics"

version := "0.1"

scalaVersion := "2.13.5"

libraryDependencies += ("com.fasterxml.jackson.core" % "jackson-core" % "2.12.2")
libraryDependencies += ("com.fasterxml.jackson.module" % "jackson-module-scala_2.13" % "2.12.2")
libraryDependencies += ("com.fasterxml.jackson.dataformat" % "jackson-dataformat-csv" % "2.12.2")
//Test
libraryDependencies += ("org.scalatest" %% "scalatest-funsuite" % "3.2.5" % "test")
